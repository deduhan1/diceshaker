﻿
namespace ConsoleEngine
{
    static class DiceSide
    {
        public static readonly int Width = 7;
        public static readonly int Height = 7; 

        private static readonly string _one = "########     ##     ##  #  ##     ##     ########";
        private static readonly string _two = "########     ##     ## # # ##     ##     ########";
        private static readonly string _three = "########     ##     ## ### ##     ##     ########";
        private static readonly string _four = "########     ## # # ##     ## # # ##     ########";
        private static readonly string _five = "########     ## # # ##  #  ## # # ##     ########";
        private static readonly string _six = "########     ## ### ##     ## ### ##     ########";  

        public static Pixel[,] One => Converter(_one);
        public static Pixel[,] Two => Converter(_two);
        public static Pixel[,] Three => Converter(_three);
        public static Pixel[,] Four => Converter(_four);
        public static Pixel[,] Five => Converter(_five);
        public static Pixel[,] Six => Converter(_six);

        public static Pixel[,] Converter(string str)
        {
            Pixel[,] array = new Pixel[Width, Height];
            int counter = 0;

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    array[i, j] = new Pixel(str[counter], ForegroundColor.RED, BackgroundColor.BLACK);
                    counter++;
                }
            }

            return array;
        }
        public static Pixel[,] Converter(string str, int width, int height)
        {
            Pixel[,] array = new Pixel[width, height];
            int counter = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    array[i, j] = new Pixel(str[counter], ForegroundColor.RED, BackgroundColor.BLACK);
                    counter++;
                }
            }

            return array;
        }
    }
}
