﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public class Dice : GameObject
    {
        private Dictionary<int, Pixel[,]> _sides = new Dictionary<int, Pixel[,]>();

        public Dice(Position position, Image image, Screen.Layer layer) : base(position, image, layer)
        {
            FillDictionary();
        }

        public override void Update()
        {

        }

        public void ChangeSide(int number)
        {
            Pixel[,] array;

            if (!_sides.TryGetValue(number, out array))
                return;

            Image = new Image(array);
        }

        private void FillDictionary()
        {
            _sides.Add(1, DiceSide.One);
            _sides.Add(2, DiceSide.Two);
            _sides.Add(3, DiceSide.Three);
            _sides.Add(4, DiceSide.Four);
            _sides.Add(5, DiceSide.Five);
            _sides.Add(6, DiceSide.Six);
        }
    }
}
