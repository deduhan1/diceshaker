﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public static class Randomizer
    {
        private static Random _random = new Random();

        public static int Range(int min, int max)
        {
            return _random.Next(min, max + 1);
        }

        public static double Range(double min, double max)
        {
            return (max - min) * _random.NextDouble() + min;
        }
    }
}
