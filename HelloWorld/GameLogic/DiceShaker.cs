﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    class DiceShaker
    {
        private List<Dice> _dices = new List<Dice>();
        private List<int> _numbers = new List<int>();

        public void Add(Dice dice)
        {
            _dices.Add(dice);
        }

        public void Shake()
        {
            _numbers.Clear();

            for (int i = 0; i < _dices.Count; i++)
            {
                int randomNumber = Randomizer.Range(1, 6);
                _numbers.Add(randomNumber);
                _dices[i].ChangeSide(randomNumber);
            }
        }

        public bool CheckResult()
        {
            return _numbers.All(n => n == 6);
        }
    }
}
