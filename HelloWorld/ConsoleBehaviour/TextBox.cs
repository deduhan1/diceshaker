﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public class TextBox : GameObject
    {
        
        public TextBox(Position position, Image image, Screen.Layer layer = Screen.Layer.Game1) : base(position, image, layer)
        {

        }
       
        public override void Update()
        {
            SetText();
        }

        private void SetText()
        {
            var hoveredPixel = Cursor.GetPixelPosition();
            string text = $"x= {hoveredPixel.X} y= {hoveredPixel.Y}";
            Image = new Image(DiceSide.Converter(text, 1, text.Length));
        }
    }
}
