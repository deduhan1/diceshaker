﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public class ScreenLayer
    {
        public Screen.Layer Layer { get; private set; }

        private Pixel[,] Array;

        public ScreenLayer(Screen.Layer layer)
        {
            Array = new Pixel[Screen.Settings.HEIGHT, Screen.Settings.WIDTH];
            Layer = layer;
        }

        public Pixel[,] GetLayer()
        {
            List<GameObject> objects = ObjectsPool.Objects.Where(obj => obj.Layer == Layer).ToList();

            Clear();
            foreach (var obj in objects)
                Add(obj);

            return Array;
        }

        private void Add(GameObject consoleObject)
        {
            int y;
            int x;

            for (int i = 0; i < consoleObject.Image.Height; i++)
            {
                y = consoleObject.Position.Y + i;

                if (y >= Screen.Settings.HEIGHT)
                    break;

                for (int j = 0; j < consoleObject.Image.Width; j++)
                {
                    x = consoleObject.Position.X + j;

                    if (x >= Screen.Settings.WIDTH)
                        break;

                    Array[y, x] = consoleObject.Image.PixelArray[i, j];
                }
            }
        }

        private void Clear()
        {
            for (int i = 0; i < Array.GetLength(0); i++)
            {
                for (int j = 0; j < Array.GetLength(1); j++)
                {
                    Array[i, j] = Pixel.Empty;
                }
            }
        }

    }
}
