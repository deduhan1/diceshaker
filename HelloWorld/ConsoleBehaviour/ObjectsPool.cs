﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public static class ObjectsPool
    {
        private static List<GameObject> _objects = new List<GameObject>();

        public static ReadOnlyCollection<GameObject> Objects => new ReadOnlyCollection<GameObject>(_objects);

        public static void Add(GameObject obj, bool enable = true)
        {
            if (_objects.Contains(obj))
                return;

            if (enable)
                obj.Enable();

            _objects.Add(obj);
        }

        public static void Remove(GameObject obj)
        {
            if (!_objects.Contains(obj))
                return;

            obj.Destroy();
            _objects.Remove(obj);
        }
    }
}
