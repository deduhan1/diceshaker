﻿
namespace ConsoleEngine
{
    public abstract class GameObject
    {
        public readonly Screen.Layer Layer;

        public Image Image {  get; protected set; }
        public Position Position { get; protected set; }
        
        public GameObject(Position position, Image image, Screen.Layer layer = Screen.Layer.Game1)
        {
            Image = image;
            Position = position;
            Layer = layer;

            ObjectsPool.Add(this);
        }

        public virtual void Update()
        {

        }

        public virtual void Enable()
        {

        }

        public virtual void Disable()
        {

        }

        public virtual void Destroy()
        {

        }
    }
}