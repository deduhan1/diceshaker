﻿
namespace ConsoleEngine
{
    public static class Screen
    {
        public enum Layer
        {
            UI,
            Game1,
            Game2,
            Game3,
            Background
        }

        public readonly static ScreenSettings Settings;
        
        private static List<ScreenLayer> _layers;

        static Screen()
        {
            Settings = new ScreenSettings(140, 70);
            _layers = CreateLayersList();
        }

        public static void Render(GameConsole gameConsole)
        {
            gameConsole.Paint(GetScreen());
        }
        public static Pixel[,] GetScreen()
        {
            Pixel[,] array = new Pixel[Settings.HEIGHT, Settings.WIDTH];

            foreach (var layer in _layers)
            {
                Pixel[,] layerArray = layer.GetLayer();

                for (int i = 0; i < layerArray.GetLength(0); i++)
                { 
                    for (int j = 0; j < layerArray.GetLength(1); j++)
                    {
                        Pixel pixel = layerArray[i, j];
                        if (pixel.Symbol == '\0' || pixel.Symbol == ' ')
                            continue;

                        array[i, j] = pixel;
                    }
                }
            }

            return array;
        }

        private static List<ScreenLayer> CreateLayersList()
        {
            List<ScreenLayer> layers = new List<ScreenLayer>();

            layers.Add(new ScreenLayer(Layer.UI));
            layers.Add(new ScreenLayer(Layer.Game1));
            layers.Add(new ScreenLayer(Layer.Game2));
            layers.Add(new ScreenLayer(Layer.Game3));
            layers.Add(new ScreenLayer(Layer.Background));

            return layers;
        }
    }
}
