﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public class Position
    {
        public int X;
        public int Y;

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Position Up => new Position(0, -1);
        public static Position Down => new Position(0, 1);
        public static Position Left => new Position(-1, 0);
        public static Position Right => new Position(1, 0);
        public static Position Zero => new Position(0, 0);

        public static Position operator +(Position first, Position second)
        {
            return new Position(first.X + second.X, first.Y + second.Y);
        }
        public static Position operator -(Position first, Position second)
        {
            return new Position(first.X - second.X, first.Y - second.Y);
        }

        public bool In(Rectangle rect)
        {
            if (X > rect.Left && X < rect.Right && Y > rect.Top && Y < rect.Bottom)
                return true;

            return false;
        }
    }
}
