﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public struct Pixel
    {
        public char Symbol { get; private set; }
        public ForegroundColor Color { get; private set; } = ForegroundColor.WHITE;
        public BackgroundColor Background { get; private set; } = BackgroundColor.BLACK;

        public Pixel(char symbol)
        {
            Symbol = symbol;
        }
        public Pixel(char symbol, ForegroundColor color)
        {
            Symbol = symbol;
            Color = color;
        }
        public Pixel(char symbol, ForegroundColor color, BackgroundColor background)
        {
            Symbol = symbol;
            Color = color;
            Background = background;
        }

        public static Pixel Empty => new Pixel('\0');
        public static Pixel Space => new Pixel(' ');

        public void ChangeColor(ForegroundColor color)
        {
            Color = color;
        }
        public void ChangeBackground(BackgroundColor background)
        {
            Background = background;
        }
    }
}
