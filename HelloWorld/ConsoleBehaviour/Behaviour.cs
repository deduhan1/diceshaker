﻿
namespace ConsoleEngine
{
    public static class Behaviour
    {
        enum Status
        {
            Run,
            Stop
        }

        private static Status _status = Status.Stop;
        private static int _renderingDelay = 20;

        public static void StartProgram()
        {
            _status = Status.Run;
            Loop();
        }

        public static void StopProgram()
        {
            _status = Status.Stop;
        }

        private static void Loop()
        {
            using (var gameConsole = new GameConsole(Screen.Settings.WIDTH, Screen.Settings.HEIGHT, new Font()))
            {
                while (_status == Status.Run)
                {
                    Cursor.Init(gameConsole);
                    Screen.Render(gameConsole);
                    ObjectsPool.Objects.ToList().ForEach(obj => obj.Update());
                    Thread.Sleep(_renderingDelay);
                }
            }
        }
    }
}
