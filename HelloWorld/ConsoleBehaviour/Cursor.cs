﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public static class Cursor
    {
        #region WinApi
        private struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Position(POINT point)
            {
                return new Position(point.X, point.Y);
            }
        }
        
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out POINT lpPoint);
        #endregion

        private static GameConsole _gameConsole;

        public static void Init(GameConsole gameConsole)
        {
            _gameConsole = gameConsole;
        }

        public static bool In()
        {
            if (_gameConsole == null)
                return false;

            Position cursor = GetCursorPosition();
            Rectangle consoleRect = _gameConsole.GetConsoleRect();
            bool isInside = cursor.In(consoleRect);
            return isInside;
        }

        public static Position GetPixelPosition()
        {
            if (_gameConsole == null)
                return Position.Zero;

            if(In() == false)
                return Position.Zero;

            var cursor = GetCursorPosition();
            var window = GameConsole.GetConsolePosition();
            Position cursorRelativeToScreen = cursor - window;
            Position hoveredPixel = new Position(cursorRelativeToScreen.X / _gameConsole.FontInfo.Width, cursorRelativeToScreen.Y / _gameConsole.FontInfo.Height);

            return hoveredPixel;
        }

        public static Position GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);
            return lpPoint;
        }
    }
}
