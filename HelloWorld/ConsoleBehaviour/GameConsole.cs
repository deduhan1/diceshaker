﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace ConsoleEngine
{
    public sealed class GameConsole : IDisposable
    {
        #region Windows API

        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern SafeFileHandle CreateFile(
            string fileName,
            [MarshalAs(UnmanagedType.U4)] uint fileAccess,
            [MarshalAs(UnmanagedType.U4)] uint fileShare,
            IntPtr securityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] int flags,
            IntPtr template);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteConsoleOutput(
            SafeFileHandle hConsoleOutput,
            CharInfo[] lpBuffer,
            Coord dwBufferSize,
            Coord dwBufferCoord,
            ref Rect lpWriteRegion);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetCurrentConsoleFontEx(
            IntPtr ConsoleOutput,
            bool MaximumWindow,
            [In, Out] ConsoleFontInfo ConsoleCurrentFontEx
        );

        [DllImport("user32.dll")]
        private static extern bool ScreenToClient(IntPtr hWnd, ref System.Drawing.Point lpPoint);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [StructLayout(LayoutKind.Sequential)]
        public struct Coord
        {
            public short X;
            public short Y;

            public Coord(short X, short Y)
            {
                this.X = X;
                this.Y = Y;
            }
        };

        [StructLayout(LayoutKind.Explicit)]
        public struct CharUnion
        {
            [FieldOffset(0)] public char UnicodeChar;
            [FieldOffset(0)] public byte AsciiChar;
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct CharInfo
        {
            [FieldOffset(0)] public CharUnion Char;
            [FieldOffset(2)] public short Attributes;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public short Left;
            public short Top;
            public short Right;
            public short Bottom;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private class ConsoleFontInfo
        {
            private int cbSize;
            public ConsoleFontInfo()
            {
                cbSize = Marshal.SizeOf(typeof(ConsoleFontInfo));
            }

            public int FontIndex;
            public short FontWidth;
            public short FontHeight;
            public int FontFamily;
            public int FontWeight;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string FaceName;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct WindowInfo
        {
            public uint cbSize;
            public Rect rcWindow;
            public Rect rcClient;
            public uint dwStyle;
            public uint dwExStyle;
            public uint dwWindowStatus;
            public uint cxWindowBorders;
            public uint cyWindowBorders;
            public ushort atomWindowType;
            public ushort wCreatorVersion;

            public WindowInfo(Boolean? filler) : this()
            {
                cbSize = (UInt32)(Marshal.SizeOf(typeof(WindowInfo)));
            }
        }

        #endregion

        private const int MF_BYCOMMAND = 0x00000000;
        private const int SC_MINIMIZE = 0xF020;
        private const int SC_MAXIMIZE = 0xF030;
        private const int SC_SIZE = 0xF000;

        public readonly Font FontInfo;

        private readonly SafeFileHandle _handler;

        public GameConsole(short width, short height, Font font = default)
        {
            _handler = CreateFile("CONOUT$", 0x40000000, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);

            if (_handler.IsInvalid)
                return;

            FontInfo = font;

            var cfi = new ConsoleFontInfo()
            {
                FaceName = font.Name,
                FontWidth = font.Width,
                FontHeight = font.Height,
                FontFamily = 0,            
                FontWeight = 0x0190,       
                FontIndex = 0
            };

            SetCurrentConsoleFontEx(_handler.DangerousGetHandle(), false, cfi);


            if (width > Console.LargestWindowWidth || height > Console.LargestWindowHeight)
                throw new InvalidOperationException($"Unable to create console; maximum width/height are {Console.LargestWindowWidth} x {Console.LargestWindowHeight}");

            Console.WindowWidth = width;
            Console.WindowHeight = height;
            Console.CursorVisible = false;
            FixConsoleSize();
        }

        public static Position GetConsolePosition()
        {
            var hWnd = GetConsoleWindow();
            System.Drawing.Point lpPoint = default;
            ScreenToClient(hWnd, ref lpPoint);
            return new Position(Math.Abs(lpPoint.X), Math.Abs(lpPoint.Y));
        }

        public Rectangle GetConsoleRect()
        {
            var consolePos = GetConsolePosition();
            var right = consolePos.X + Screen.Settings.WIDTH * FontInfo.Width;
            var bottom = consolePos.Y + Screen.Settings.HEIGHT * FontInfo.Height;
            Rectangle rect = new Rectangle(consolePos.X, consolePos.Y, right, bottom);
            return rect;
        }
        public void Paint(Pixel[,] pixels)
        {
            if (_handler.IsInvalid)
                return;

            CharInfo[] buf = Convert(pixels);
            Rect rect = new Rect() { Left = 0, Top = 0, Right = Screen.Settings.WIDTH, Bottom = Screen.Settings.WIDTH };

            bool b = WriteConsoleOutput(_handler, buf,
              new Coord() { X = Screen.Settings.WIDTH, Y = Screen.Settings.HEIGHT },
              new Coord() { X = 0, Y = 0 },
              ref rect);
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        private CharInfo[] Convert(Pixel[,] pixels)
        {
            CharInfo[] ci = new CharInfo[pixels.Length];
            int counter = 0;
            string str = "";
            foreach (var pixel in pixels)
                str += pixel.Symbol;

            byte[] b = Encoding.ASCII.GetBytes(str);

            foreach (var pixel in pixels)
            {
                ci[counter].Attributes = (short)((short)pixel.Color + (short)pixel.Background);
                ci[counter].Char.AsciiChar = b[counter];
                counter++;
            }

            return ci;
        }
        private void FixConsoleSize()
        {
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_MINIMIZE, MF_BYCOMMAND);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_MAXIMIZE, MF_BYCOMMAND);
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_SIZE, MF_BYCOMMAND);
        }
    }
}
