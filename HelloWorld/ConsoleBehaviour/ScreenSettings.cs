﻿
namespace ConsoleEngine
{
    public struct ScreenSettings
    {
        public readonly short WIDTH;
        public readonly short HEIGHT;

        public ScreenSettings()
        {
            WIDTH = 50;
            HEIGHT = 50;
        }

        public ScreenSettings(short width, short height)
        {
            WIDTH = width;
            HEIGHT = height;
        }
    }
}
