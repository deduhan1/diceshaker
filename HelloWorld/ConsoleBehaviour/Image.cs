﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public class Image
    {
        public readonly int Height;
        public readonly int Width;
        public readonly Pixel[,] PixelArray;

        public ForegroundColor Color { get; private set; }
        public BackgroundColor Background { get; private set; }

        public Image(Pixel[,] array)
        {
            Height = array.GetLength(0);
            Width = array.GetLength(1);
            PixelArray = array;
        }

        public Image(Pixel[,] array, ForegroundColor color)
        {
            Height = array.GetLength(0);
            Width = array.GetLength(1);
            PixelArray = array;
            Color = color;
        }

        public Image(Pixel[,] array, ForegroundColor color, BackgroundColor background)
        {
            Height = array.GetLength(0);
            Width = array.GetLength(1);
            PixelArray = array;
            Color = color;
            Background = background;
        }

        public void ChangeColor(ForegroundColor color)
        {
            if (Color == color)
                return;

            Color = color;
            foreach (var pixel in PixelArray)
                pixel.ChangeColor(Color);
        }

        public void ChangeBackground(BackgroundColor background)
        {
            if (Background == background)
                return;

            Background = background;
            foreach (var pixel in PixelArray)
                pixel.ChangeBackground(Background);
        }
    }
}
