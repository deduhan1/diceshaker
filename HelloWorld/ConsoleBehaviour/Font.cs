﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEngine
{
    public struct Font
    {
        public readonly string Name;
        public readonly short Width;
        public readonly short Height;

        public Font()
        {
            Name = "Consolas";
            Width = 8;
            Height = 8;
        }

        public Font(string name, short width, short height)
        {
            Name = name;
            Width = width;
            Height = height;
        }
    }
}
