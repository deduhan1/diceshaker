﻿
namespace ConsoleEngine
{
    class Program
    {
        public static void Main(string[] args)
        {
            Dice dice = new Dice(new Position(0, 5), new Image(DiceSide.Five), Screen.Layer.Game1);
            TextBox textBox = new TextBox(Position.Zero, new Image(new Pixel[1, 10]), Screen.Layer.UI);
            Behaviour.StartProgram();

            Console.ReadKey();
        }
    }
}
